using Coin.Sdk.WM.Messages.V1;
using Coin.Sdk.WM.Service.Impl;
using NUnit.Framework;
using static Coin.Sdk.WM.Tests.TestSettings;
using static Coin.Sdk.WM.Tests.TestUtils;

namespace Coin.Sdk.WM.Tests;

public class WeasMigrationServiceTests
{
    private WeasMigrationService _weasMigrationService = null!;

    [SetUp]
    public void Setup()
    {
        _weasMigrationService = new WeasMigrationService(ApiUrl, Consumer, PrivateKeyFile, EncryptedHmacSecretFile);
    }

    private MessageResponse SendWeasMigrationRequest(string dossierId)
    {
        var message = new MessageEnvelope<WeasMigrationRequest>
        {
            Message = new WeasMigrationRequestMessage
            {
                Header = new Header
                {
                    Sender = new Sender
                    {
                        ServiceProvider = Recipient
                    },
                    Receiver = new Receiver
                    {
                        ServiceProvider = Donor
                    },
                    Timestamp = Timestamp
                },
                Body = new WeasMigrationRequestBody
                {
                    Content = new WeasMigrationRequest
                    {
                        DossierId = dossierId,
                        RecipientServiceProvider = Recipient,
                        DonorServiceProvider = Donor,
                        EarlyTermination = "N",
                        Name = "Vereniging COIN",
                        AddressBlock = new AddressBlock
                        {
                            Housenr = "123",
                            HousenrExt = "A",
                            Postcode = "1234AB"
                        },
                        NumberSeries =
                        [
                            new NumberSeries
                            {
                                Start = PhoneNumber,
                                End = PhoneNumber
                            }
                        ],
                        ValidationBlock =
                        [
                            new ValidationBlock
                            {
                                Name = "sdfn33-fdin",
                                Value = "gdsf-45n"
                            }
                        ],
                        Note = "Some note"
                    }
                }
            }
        };
        return _weasMigrationService.SendMessageAsync(message).Result;
    }

    [Test]
    public void SendWeasMigrationRequest()
    {
        var dossierId = GenerateDossierId(Recipient, Donor);
        var response = SendWeasMigrationRequest(dossierId);
        Assert.That(response, Is.Not.InstanceOf<ErrorResponse>());
    }

    [Test]
    public void SendInvalidWeasMigrationRequest()
    {
        var dossierId = GenerateDossierId(Recipient, Donor);
        var response = SendWeasMigrationRequest(dossierId + "invalid");
        Assert.That(response, Is.InstanceOf<ErrorResponse>());
    }
}