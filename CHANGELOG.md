# Changelog

## Version 1.7.1

Changed:

- Target framework back to .NET Standard 2.0, except for Mobile Connect SDK

## Version 1.7.0

Added:

- Mobile Connect SDK


## Version 1.6.0

Added:

- WEAS Migration SDK

Changed:

- Framework updates (.NET Standard 2.1, .NET 8.0)
- Some library updates
- Included query parameters in HMAC computation (security improvement)

## Version 1.4.0

Added:

- Support for Bundle Switching v5; removes the business field from contract termination requests
and contract termination request answers

## Version 1.3.0

Added:

- Support for Number Portability v3; adds the contract field to porting requests
