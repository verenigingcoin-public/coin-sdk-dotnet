using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Coin.Sdk.WM.Messages.V1;

public interface IMessageEnvelope<out T> where T : IWmMessageContent
{
    IWmMessage<T> Message { get; }
}

public interface IWmMessage<out T> where T : IWmMessageContent
{
    Header Header { get; set; }
    IWmMessageBody<T> Body { get; }
}

public interface IWmMessageBody<out T> where T : IWmMessageContent
{
    T Content { get; }
}

public interface IWmMessageContent
{
    string DossierId { get; set; }
}

public class MessageEnvelope<T> : IMessageEnvelope<T> where T : IWmMessageContent
{
    [DataMember(Name = "message", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "message")]
    public IWmMessage<T> Message { get; set; }
}