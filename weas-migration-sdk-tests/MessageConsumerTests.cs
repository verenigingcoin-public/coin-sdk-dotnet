﻿using System.Threading;
using Coin.Sdk.Common.Client;
using Coin.Sdk.WM.Messages.V1;
using Coin.Sdk.WM.Service.Impl;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;
using static Coin.Sdk.WM.Tests.TestSettings;

namespace Coin.Sdk.WM.Tests;

public class MessageConsumerTests
{
    private WeasMigrationMessageConsumer _messageConsumer = null!;
    private TestListener _listener = null!;

    [SetUp]
    public void Setup()
    {
        var logger = NullLogger.Instance;

        var sseConsumer = new SseConsumer(logger, Consumer, SseUrl, PrivateKeyFile, EncryptedHmacSecretFile);
        _messageConsumer = new WeasMigrationMessageConsumer(sseConsumer, logger);
        _listener = new TestListener();
    }

    [Test, CancelAfter(5000)]
    public void ConsumeUnconfirmed()
    {
        var cde = new CountdownEvent(5);
        _listener.SideEffect = _ => cde.Signal();
        _messageConsumer.StartConsumingUnconfirmed(_listener, _ => Assert.Fail("Disconnected"));
        cde.Wait();
        _messageConsumer.StopConsuming();
    }

    [Test, CancelAfter(5000)]
    public void ConsumeAllFiltered()
    {
        var cde = new CountdownEvent(3);
        _listener.SideEffect = _ => cde.Signal();
        _messageConsumer.StartConsumingAll(_listener, new TestOffsetPersister(), 3,
            _ => Assert.Fail("Disconnected"), null,
            MessageType.WeasMigrationRequestV1);
        cde.Wait();
        _messageConsumer.StopConsuming();
    }
}