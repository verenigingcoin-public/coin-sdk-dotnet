using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Coin.Sdk.WM.Messages.V1;

public class WeasMigrationRequestMessage : IWmMessage<WeasMigrationRequest>
{
    [DataMember(Name = "header", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "header")]
    public Header Header { get; set; }

    [DataMember(Name = "body", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "body")]
    [JsonConverter(typeof(ConcreteConverter<WeasMigrationRequestBody>))]
    public IWmMessageBody<WeasMigrationRequest> Body { get; set; }
}

public class WeasMigrationRequestBody : IWmMessageBody<WeasMigrationRequest>
{
    [DataMember(Name = "contractterminationrequest", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "contractterminationrequest")]
    public WeasMigrationRequest Content { get; set; }
}

public class WeasMigrationRequest : IWmMessageContent
{
    /// <summary>
    /// The identifier for the WEAS Migration Request determined by the Recipient for communication between the Recipient and Donor. Defined as [RecipientSPCode]-[DonorSPCode]-[Identifier] The requirement is that the DossierId is unique and identifies the WEAS Migration Request. 
    /// </summary>
    [DataMember(Name = "dossierid", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "dossierid")]
    public string DossierId { get; set; }

    /// <summary>
    /// Code of the Recipient party
    /// </summary>
    [DataMember(Name = "recipientserviceprovider", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "recipientserviceprovider")]
    public string RecipientServiceProvider { get; set; }

    /// <summary>
    /// Code of the Recipient party
    /// </summary>
    [DataMember(Name = "recipientnetworkoperator", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "recipientnetworkoperator")]
    public string RecipientNetworkOperator { get; set; }

    /// <summary>
    /// Code of the Donor party
    /// </summary>
    [DataMember(Name = "donornetworkoperator", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "donornetworkoperator")]
    public string DonorNetworkOperator { get; set; }

    /// <summary>
    /// Code of the Donor party 
    /// </summary>
    [DataMember(Name = "donorserviceprovider", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "donorserviceprovider")]
    public string DonorServiceProvider { get; set; }

    /// <summary>
    /// Indicates whether there is an authorisation for early termination of contract
    /// </summary>
    [DataMember(Name = "earlytermination", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "earlytermination")]
    public string EarlyTermination { get; set; }

    /// <summary>
    /// Contract name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    [DataMember(Name = "addressblock", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "addressblock")]
    public AddressBlock AddressBlock { get; set; }

    [DataMember(Name = "numberseries", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "numberseries")]
    public List<NumberSeries> NumberSeries { get; set; }

    [DataMember(Name = "validationblock", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "validationblock")]
    public List<ValidationBlock> ValidationBlock { get; set; }

    /// <summary>
    /// Note field for additional information
    /// </summary>
    [DataMember(Name = "note", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "note")]
    public string Note { get; set; }
}

public class ValidationBlock
{
    /// <summary>
    /// Contract identification (contractid or iban) that is requested to be terminated
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "name")]
    public string Name { get; set; }

    /// <summary>
    /// Value of the contract identification (contractid or iban) that is requested to be terminated 
    /// </summary>
    [DataMember(Name = "value", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "value")]
    public string Value { get; set; }
}

public class AddressBlock
{
    [DataMember(Name = "postcode", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "postcode")]
    public string Postcode { get; set; }

    [DataMember(Name = "housenr", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "housenr")]
    public string Housenr { get; set; }

    [DataMember(Name = "housenr_ext", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "housenr_ext")]
    public string HousenrExt { get; set; }
}