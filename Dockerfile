FROM mcr.microsoft.com/dotnet/sdk:8.0
WORKDIR /app

COPY coin-sdk-dotnet.sln ./
COPY common-sdk/ ./common-sdk/
COPY number-portability-sdk/ ./number-portability-sdk/
COPY number-portability-sdk-tests/ ./number-portability-sdk-tests/
COPY bundle-switching-sdk/ ./bundle-switching-sdk/
COPY bundle-switching-sdk-tests/ ./bundle-switching-sdk-tests/
COPY mobile-connect-sdk/ ./mobile-connect-sdk/
COPY mobile-connect-sdk-tests/ ./mobile-connect-sdk-tests/
COPY keys/ ./keys/

RUN dotnet build common-sdk && dotnet build number-portability-sdk && dotnet build bundle-switching-sdk && dotnet build mobile-connect-sdk
CMD dotnet test number-portability-sdk-tests && dotnet test bundle-switching-sdk-tests && dotnet test mobile-connect-sdk-tests
