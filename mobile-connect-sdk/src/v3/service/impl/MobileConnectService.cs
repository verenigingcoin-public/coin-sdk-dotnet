using System;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using Coin.Sdk.Common.Client;
using Coin.Sdk.MC.v3.domain;
using Newtonsoft.Json.Linq;
using static Coin.Sdk.Common.Crypto.CtpApiClientUtil;

namespace Coin.Sdk.MC.v3.service.impl;

public class MobileConnectService(
    Uri discoveryUrl,
    string consumerName,
    HMACSHA256 signer,
    RSA privateKey,
    IMobileConnectClientErrorHandler errorHandler
) : CtpApiRestTemplateSupport(consumerName, privateKey, signer), IMobileConnectService
{
    public MobileConnectService(string apiUrl, string consumerName, string privateKeyFile,
        string encryptedHmacSecretFile,
        IMobileConnectClientErrorHandler errorHandler, string? privateKeyFilePassword = null)
        : this(new Uri(apiUrl), consumerName, privateKeyFile, encryptedHmacSecretFile, errorHandler,
            privateKeyFilePassword)
    {
    }

    public MobileConnectService(Uri apiUrl, string consumerName, string privateKeyFile, string encryptedHmacSecretFile,
        IMobileConnectClientErrorHandler errorHandler,
        string? privateKeyFilePassword = null)
        : this(apiUrl, consumerName, ReadPrivateKeyFile(privateKeyFile, privateKeyFilePassword),
            encryptedHmacSecretFile, errorHandler)
    {
    }

    public MobileConnectService(string apiUrl, string consumerName, RSA privateKey, string encryptedHmacSecretFile,
        IMobileConnectClientErrorHandler errorHandler)
        : this(new Uri(apiUrl), consumerName, privateKey, encryptedHmacSecretFile, errorHandler)
    {
    }

    public MobileConnectService(Uri apiUrl, string consumerName, RSA privateKey, string encryptedHmacSecretFile,
        IMobileConnectClientErrorHandler errorHandler)
        : this(apiUrl, consumerName, HmacFromEncryptedBase64EncodedSecretFile(encryptedHmacSecretFile, privateKey),
            privateKey, errorHandler)
    {
    }

    public MobileConnectService(string apiUrl, string consumerName, HMACSHA256 signer, RSA privateKey,
        IMobileConnectClientErrorHandler errorHandler)
        : this(new Uri(apiUrl), consumerName, signer, privateKey, errorHandler)
    {
    }

    public async Task<DiscoveryResponse?> SendDiscoveryRequestAsync(DiscoveryRequest discoveryRequest,
        CancellationToken cancellationToken = default)
    {
        var responseMessage =
            await SendWithTokenAsync(HttpMethod.Post, discoveryUrl, discoveryRequest, cancellationToken)
                .ConfigureAwait(false);
        var responseBody = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
        var json = JObject.Parse(responseBody);
        if (responseMessage.IsSuccessStatusCode)
        {
            return json.ToObject<DiscoveryResponse>();
        }

        if (responseMessage.StatusCode.Equals(HttpStatusCode.NotFound))
        {
            ErrorResponse? errorResponse = null;
            try
            {
                errorResponse = json.ToObject<ErrorResponse>();
            }
            catch (Exception)
            {
                // ignored
            }

            if (errorResponse != null)
            {
                errorHandler.OnNotFound(discoveryRequest, errorResponse);
                return null;
            }
        }

        errorHandler.OnOtherError(discoveryRequest, responseMessage, responseBody);
        return null;
    }
}