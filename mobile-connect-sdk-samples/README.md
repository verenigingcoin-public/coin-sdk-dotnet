# COIN Mobile Connect .NET SDK Samples

For extensive documentation on the .NET Mobile Connect SDK, please have a look at the following [README](../mobile-connect-sdk/README.md).

# Configure Example
In order to successfully run the tests, `private-key.pem` and `sharedkey.encrypted` have to be located in this directory.
Furthermore, the correct consumer name should be assigned to the variable `consumer` in  the `Tests` class.
