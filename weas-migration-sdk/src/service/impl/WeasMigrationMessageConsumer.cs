﻿using System;
using System.Collections.Generic;
using System.Linq;
using Coin.Sdk.Common.Client;
using Coin.Sdk.WM.Messages.V1;
using EvtSource;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Coin.Sdk.WM.Service.Impl;

public class WeasMigrationMessageConsumer(SseConsumer sseConsumer, ILogger logger)
{
    private const long DefaultOffset = -1;

    public void StopConsuming()
    {
        sseConsumer.StopConsuming();
    }

    /// <summary>
    /// Recommended method for consuming messages. On connect or reconnect it will consume all unconfirmed messages.
    /// <br/><br/>
    /// Only provide serviceProviders if there are multiple service providers linked to your consumer
    /// and you only want to consume messages for a subset of these providers.
    /// </summary>
    public void StartConsumingUnconfirmed(
        IWeasMigrationMessageListener listener,
        Action<Exception> onFinalDisconnect = null,
        IEnumerable<string> serviceProviders = null,
        params MessageType[] messageTypes
    )
    {
        sseConsumer.StartConsumingUnconfirmed(
            sse => HandleSse(listener, sse),
            messageTypes.Select(m => m.Name()),
            new Dictionary<string, IEnumerable<string>> { ["serviceprovider"] = serviceProviders },
            onFinalDisconnect);
    }

    /// <summary>
    /// Consume all messages, both confirmed and unconfirmed, from a certain offset.
    /// Only use for special cases if <see cref="StartConsumingUnconfirmed"/> does not meet needs.
    /// <br/><br/>
    /// Only provide serviceProviders if there are multiple service providers linked to your consumer
    /// and you only want to consume messages for a subset of these providers.
    /// </summary>
    public void StartConsumingAll(
        IWeasMigrationMessageListener listener,
        IOffsetPersister offsetPersister,
        long offset = DefaultOffset,
        Action<Exception> onFinalDisconnect = null,
        IEnumerable<string> serviceProviders = null,
        params MessageType[] messageTypes
    )
    {
        sseConsumer.StartConsumingAll(
            sse => HandleSse(listener, sse),
            offsetPersister,
            offset,
            messageTypes.Select(m => m.Name()),
            new Dictionary<string, IEnumerable<string>> { ["serviceprovider"] = serviceProviders },
            onFinalDisconnect);
    }

    /// <summary>
    /// Only use this method for receiving unconfirmed messages if you make sure that all messages that are received
    /// through this method will be confirmed otherwise, ideally in the stream opened by
    /// <see cref="StartConsumingUnconfirmed"/>. So this method should only be used for a secondary
    /// stream (e.g. backoffice process) that needs to consume unconfirmed messages for administrative purposes.
    /// <br/><br/>
    /// Only provide serviceProviders if there are multiple service providers linked to your consumer
    /// and you only want to consume messages for a subset of these providers.
    /// </summary>
    public void StartConsumingUnconfirmedWithOffsetPersistence(
        IWeasMigrationMessageListener listener,
        IOffsetPersister offsetPersister,
        long offset = DefaultOffset,
        Action<Exception> onFinalDisconnect = null,
        IEnumerable<string> serviceProviders = null,
        params MessageType[] messageTypes
    )
    {
        sseConsumer.StartConsumingUnconfirmedWithOffsetPersistence(
            sse => HandleSse(listener, sse),
            offsetPersister,
            offset,
            messageTypes.Select(m => m.Name()),
            new Dictionary<string, IEnumerable<string>> { ["serviceprovider"] = serviceProviders },
            onFinalDisconnect);
    }

    private bool HandleSse(IWeasMigrationMessageListener listener, EventSourceMessageEventArgs eventArgs)
    {
        try
        {
            switch (eventArgs.Event)
            {
                case "weasmigrationcancel-v1":
                    listener.OnWeasMigrationCancel(eventArgs.Id, GetWmMessage<WeasMigrationCancelMessage>(eventArgs));
                    return true;
                case "weasmigrationerrorfound-v1":
                    listener.OnWeasMigrationErrorFound(eventArgs.Id, GetWmMessage<WeasMigrationErrorFoundMessage>(eventArgs));
                    return true;
                case "weasmigrationrequest-v1":
                    listener.OnWeasMigrationRequest(eventArgs.Id, GetWmMessage<WeasMigrationRequestMessage>(eventArgs));
                    return true;
                case "weasmigrationrequestanswer-v1":
                    listener.OnWeasMigrationRequestAnswer(eventArgs.Id, GetWmMessage<WeasMigrationRequestAnswerMessage>(eventArgs));
                    return true;
                default:
                    if (string.IsNullOrWhiteSpace(eventArgs.Message))
                    {
                        listener.OnKeepAlive();
                        return true;
                    }
                    listener.OnUnknownMessage(eventArgs.Id, eventArgs.Message);
                    return true;
            }
        }
#pragma warning disable CA1031 // Do not catch general exception types
        catch (Exception ex)
#pragma warning restore CA1031 // Do not catch general exception types
        {
            logger.LogError(ex, @"An error occured");
            listener.OnException(ex);
            return false;
        }
    }

    private static T GetWmMessage<T>(EventSourceMessageEventArgs eventArgs)
    {
        return JObject.Parse(eventArgs.Message).First.First.ToObject<T>();
    }
}