using System.Collections.Generic;
using Newtonsoft.Json;

namespace Coin.Sdk.MC.v3.domain;

public record DiscoveryRequest(string Msisdn, string? CorrelationId)
{
    [JsonProperty(PropertyName = "msisdn")]
    public string Msisdn { get; } = Msisdn;
    [JsonProperty(PropertyName = "correlationId")]
    public string? CorrelationId { get; } = CorrelationId;
}

public record DiscoveryResponse(
    string NetworkOperatorCode,
    SupportedServices SupportedServices,
    string? CorrelationId,
    string? ClientId,
    string? ClientSecret
)
{
    public string NetworkOperatorCode { get; } = NetworkOperatorCode;
    public SupportedServices SupportedServices { get; } = SupportedServices;
    public string? CorrelationId { get; } = CorrelationId;
    public string? ClientId { get; } = ClientId;
    public string? ClientSecret { get; } = ClientSecret;
}

public record SupportedServices(
    HashSet<Link> Match,
    HashSet<Link> NumberVerify,
    HashSet<Link> AccountTakeoverProtection
)
{
    public HashSet<Link> Match { get; } = Match;
    public HashSet<Link> NumberVerify { get; } = NumberVerify;
    public HashSet<Link> AccountTakeoverProtection { get; } = AccountTakeoverProtection;
}

public record Link(string Rel, string Href)
{
    public string Rel { get; } = Rel;
    public string Href { get; } = Href;
}

public record ErrorResponse(
    string Error,
    string Description,
    [JsonProperty(PropertyName = "correlation_id")]
    string? CorrelationId)
{
    public string Error { get; } = Error;
    public string Description { get; } = Description;
    public string? CorrelationId { get; } = CorrelationId;
}