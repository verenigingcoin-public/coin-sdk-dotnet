using Coin.Sdk.MC.v3.domain;
using Coin.Sdk.MC.v3.service;
using Coin.Sdk.MC.v3.service.impl;
using Moq;
using NUnit.Framework;

namespace Coin.Sdk.MC.Tests;

public class DiscoveryCustomErrorHandlerTest
{
    private Mock<IMobileConnectClientErrorHandler> _errorHandlerMock;
    private IMobileConnectService _mobileConnectService;

    [SetUp]
    public void Setup()
    {
        _errorHandlerMock = new Mock<IMobileConnectClientErrorHandler>();
        _mobileConnectService = new MobileConnectService(TestSettings.DiscoveryUrl, TestSettings.Consumer,
            TestSettings.PrivateKeyFile, TestSettings.EncryptedHmacSecretFile, _errorHandlerMock.Object);
    }

    [Test]
    public async Task SendDiscoveryRequest()
    {
        Assert.That(await _mobileConnectService.SendDiscoveryRequestAsync("123456789"), Is.Not.Null);
        _errorHandlerMock.VerifyNoOtherCalls();
    }

    [Test]
    public async Task SendDiscoveryRequestNotFound()
    {
        DiscoveryRequest request = new DiscoveryRequest("123456789", "404");
        Assert.That(await _mobileConnectService.SendDiscoveryRequestAsync(request), Is.Null);
        _errorHandlerMock.Verify(handler => handler.OnNotFound(request, It.IsAny<ErrorResponse>()), Times.Once);
    }

    [Test]
    public async Task SendDiscoveryRequestOtherError()
    {
        DiscoveryRequest request = new DiscoveryRequest("123456789", "403");
        Assert.That(await _mobileConnectService.SendDiscoveryRequestAsync(request), Is.Null);
        _errorHandlerMock.Verify(
            handler => handler.OnOtherError(request, It.IsAny<HttpResponseMessage>(), It.IsAny<string>()), Times.Once);
    }
}