﻿using System.Threading.Tasks;
using Coin.Sdk.MC.v3.service;
using Coin.Sdk.MC.v3.service.impl;
using NUnit.Framework;
using static Coin.Sdk.MC.Sample.TestUtils;

namespace Coin.Sdk.MC.Sample;

public class Tests
{
    private IMobileConnectService _mobileConnectService;
    private const string Msisdn = "31610101010";

    [SetUp]
    public void Setup()
    {
        const string discoveryUrl = "https://test-api.coin.nl/mobile-connect/v3/discovery";
        const string consumer = "<YOUR CONSUMER>";
        var privateKeyFile = GetPath("private-key.pem");
        var encryptedHmacSecretFile = GetPath("sharedkey.encrypted");
        var errorHandler = new MobileConnectClientErrorHandler();
        _mobileConnectService =
            new MobileConnectService(discoveryUrl, consumer, privateKeyFile, encryptedHmacSecretFile, errorHandler);
    }

    [Test]
    public async Task SendDiscoveryRequest()
    {
        var response = await _mobileConnectService.SendDiscoveryRequestAsync(Msisdn);
        System.Console.WriteLine(response);
    }
}