using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Coin.Sdk.WM.Messages.V1;

namespace Coin.Sdk.WM.Service;

public interface IWeasMigrationMessageListener
{
    void OnKeepAlive();

    void OnException(Exception exception);

    void OnUnknownMessage(string messageId, string message);

    void OnWeasMigrationRequest(string messageId, WeasMigrationRequestMessage message);

    void OnWeasMigrationRequestAnswer(string messageId, WeasMigrationRequestAnswerMessage message);

    void OnWeasMigrationCancel(string messageId, WeasMigrationCancelMessage message);

    void OnWeasMigrationErrorFound(string messageId, WeasMigrationErrorFoundMessage message);
}

public interface IWeasMigrationService
{
    Task<HttpResponseMessage> SendConfirmationAsync(string id, CancellationToken cancellationToken = default);

    Task<MessageResponse> SendMessageAsync(IMessageEnvelope<IWmMessageContent> message,
        CancellationToken cancellationToken = default);
}