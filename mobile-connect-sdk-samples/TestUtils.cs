namespace Coin.Sdk.MC.Sample;

public static class TestUtils
{
    public static string GetPath(string relativePath) => $"../../../{relativePath}";
}