# .NET Mobile Connect SDK

## Introduction

This SDK supports secured access to the Mobile Connect API.

## Setup

### Samples Project for the Mobile Connect API
A sample project is provided in the `mobile-connect-sdk-samples` directory.

### NuGet Package
This SDK is published as the NuGet package 'Vereniging-COIN.Sdk.MC'.

## Configure Credentials

For secure access credentials are required.
- Check [this README](https://gitlab.com/verenigingcoin-public/consumer-configuration/-/blob/master/README.md) to find out how to configure these.
- In summary you will need:
    - a consumer name
    - a private key file (or a `System.Security.Cryptography.RSA` instance containing this key)
    - a file containing the encrypted Hmac secret
    (or a `System.Security.Cryptography.HMACSHA256` instance containing this (decrypted) secret)

## Code
The `MobileConnectService.SendDiscoveryRequestAsync()` methods can be used to send discovery requests.
To create this service, the aforementioned credentials are needed.
In addition, its constructor requires an `IMobileConnectClientErrorHandler`.
For this, you can create a custom implementation or use the provided `MobileConnectClientErrorHandler`.
This handler ignores `404 not found` errors from the api, which results in the `SendDiscoveryRequestAsync()` method
returning (a `Task` that completes with) `null`. Other errors result in an exception being thrown.
