using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Coin.Sdk.WM.Messages.V1;

public static class Utils
{
    public static string TypeName<T>() where T : IWmMessageContent, new() => TypeName(new T());

    public static string SimpleTypeName(IMessageEnvelope<IWmMessageContent> envelope) =>
        TypeName(envelope).Replace("weasmigration", String.Empty);

    public static string TypeName(IMessageEnvelope<IWmMessageContent> envelope)
    {
        if (envelope is null)
            throw new ArgumentNullException(nameof(envelope));
        return TypeName(envelope.Message.Body.Content);
    }

    public static string TypeName(IWmMessage<IWmMessageContent> message)
    {
        if (message is null)
            throw new ArgumentNullException(nameof(message));
        return TypeName(message.Body.Content);
    }

    public static string TypeName(IWmMessageBody<IWmMessageContent> body)
    {
        if (body is null)
            throw new ArgumentNullException(nameof(body));
        return TypeName(body.Content);
    }

    public static string TypeName(IWmMessageContent content)
    {
        if (content is null)
            throw new ArgumentNullException(nameof(content));
#pragma warning disable CA1308 // Normalize strings to uppercase
        return content.GetType().Name.Split('.').Last().ToLowerInvariant().Replace("message", string.Empty);
#pragma warning restore CA1308 // Normalize strings to uppercase
    }

    public static IWmMessage<IWmMessageContent> Deserialize(string type, string json)
    {
        var message = JObject.Parse(json).First.First;
        switch (type)
        {
            case "weasmigrationcancel-v1": return message.ToObject<WeasMigrationCancelMessage>();
            case "weasmigrationerrorfound-v1": return message.ToObject<WeasMigrationErrorFoundMessage>();
            case "weasmigrationrequest-v1": return message.ToObject<WeasMigrationRequestMessage>();
            case "weasmigrationrequestanswer-v1": return message.ToObject<WeasMigrationRequestAnswerMessage>();
            default: throw new JsonException($"Unknown message type {type}");
        }
    }
}

public class ConcreteConverter<T> : JsonConverter
{
    public override bool CanConvert(Type objectType) => true;

    public override object ReadJson(JsonReader reader,
        Type objectType, object existingValue, JsonSerializer serializer)
    {
        if (serializer is null)
            throw new ArgumentNullException(nameof(serializer));
        return serializer.Deserialize<T>(reader);
    }

    public override void WriteJson(JsonWriter writer,
        object value, JsonSerializer serializer)
    {
        if (serializer is null)
            throw new ArgumentNullException(nameof(serializer));
        serializer.Serialize(writer, value);
    }
}