using System;
using System.Diagnostics;
using Coin.Sdk.Common.Client;
using Coin.Sdk.WM.Messages.V1;
using Coin.Sdk.WM.Service;

namespace Coin.Sdk.WM.Tests;

public static class TestUtils
{
    public static string GenerateDossierId(string recipient, string donor) =>
        $"{recipient}-{donor}-{new Random().Next(10000, 99999)}-1";
}

public static class TestSettings
{
    public const string PrivateKeyFile = "../../../../keys/private-key.pem";
    public const string EncryptedHmacSecretFile = "../../../../keys/sharedkey.encrypted";

    public static readonly string ApiUrl =
        "http://" + (Environment.GetEnvironmentVariable("STUB_HOST_AND_PORT") ?? "localhost:8000") +
        "/weas-migration/v1";

    public static readonly string SseUrl = ApiUrl + "/dossiers/events";

    public const string Consumer = "loadtest-loada";
    public const string Recipient = "LOADA";
    public const string Donor = "LOADB";
    public const string PhoneNumber = "0612345678";
    public static readonly string Timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
}

public class TestListener : IWeasMigrationMessageListener
{
    private Action<string> _sideEffect = _ => { };

    public Action<string> SideEffect
    {
        set => _sideEffect = value;
    }

    public void OnWeasMigrationCancel(string messageId, WeasMigrationCancelMessage message)
    {
        Debug.WriteLine($"Received message with id {messageId} of type {message.GetType()}");
        _sideEffect(messageId);
    }

    public void OnWeasMigrationErrorFound(string messageId, WeasMigrationErrorFoundMessage message)
    {
        Debug.WriteLine($"Received message with id {messageId} of type {message.GetType()}");
        _sideEffect(messageId);
    }

    public void OnException(Exception exception)
    {
        Debug.WriteLine($"Exception received {exception}");
    }

    public void OnKeepAlive()
    {
        Debug.WriteLine("Keepalive");
    }

    public void OnWeasMigrationRequest(string messageId, WeasMigrationRequestMessage message)
    {
        Debug.WriteLine($"Received message with id {messageId} of type {message.GetType()}");
        _sideEffect(messageId);
    }

    public void OnWeasMigrationRequestAnswer(string messageId,
        WeasMigrationRequestAnswerMessage message)
    {
        Debug.WriteLine($"Received message with id {messageId} of type {message.GetType()}");
        _sideEffect(messageId);
    }

    public void OnUnknownMessage(string messageId, string message)
    {
        Debug.WriteLine($"Unknown message received: {message}");
    }
}

public class TestOffsetPersister : IOffsetPersister
{
    public long Offset { get; set; }
}