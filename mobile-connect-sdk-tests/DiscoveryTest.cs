using System.Net;
using Coin.Sdk.MC.v3.service;
using Coin.Sdk.MC.v3.service.impl;
using NUnit.Framework;
using static Coin.Sdk.MC.Tests.TestSettings;

namespace Coin.Sdk.MC.Tests;

public class DiscoveryTest
{
    private readonly IMobileConnectService _mobileConnectService = new MobileConnectService(DiscoveryUrl, Consumer,
        PrivateKeyFile, EncryptedHmacSecretFile, new MobileConnectClientErrorHandler());

    [Test]
    public async Task SendDiscoveryRequest()
    {
        Assert.That(await _mobileConnectService.SendDiscoveryRequestAsync("123456789"), Is.Not.Null);
    }

    [Test]
    public async Task SendDiscoveryRequestNotFound()
    {
        Assert.That(await _mobileConnectService.SendDiscoveryRequestAsync("123456789", "404"), Is.Null);
    }

    [Test]
    public void SendDiscoveryRequestOtherError()
    {
        Assert.ThrowsAsync<HttpListenerException>(() => _mobileConnectService.SendDiscoveryRequestAsync("123456789", "403"));
    }
}