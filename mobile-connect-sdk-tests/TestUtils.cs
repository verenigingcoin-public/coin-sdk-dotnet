using System.Diagnostics;
using Coin.Sdk.Common.Client;

namespace Coin.Sdk.MC.Tests;

public static class TestSettings
{
    public const string PrivateKeyFile = "../../../../keys/private-key.pem";
    public const string EncryptedHmacSecretFile = "../../../../keys/sharedkey.encrypted";

    public static readonly string DiscoveryUrl =
        "http://" + (Environment.GetEnvironmentVariable("STUB_HOST_AND_PORT") ?? "localhost:8000") +
        "/mobile-connect/v3/discovery";

    public const string Consumer = "loadtest-loada";
    public static readonly string Timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
}