using Coin.Sdk.BS.Messages.V5;
using Coin.Sdk.Common;
using Coin.Sdk.Common.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using static Coin.Sdk.BS.Messages.V5.Utils;
using static Coin.Sdk.Common.Crypto.CtpApiClientUtil;

namespace Coin.Sdk.BS.Service.Impl;

public class BundleSwitchingService(Uri apiUrl, string consumerName, HMACSHA256 signer, RSA privateKey)
    : CtpApiRestTemplateSupport(consumerName, privateKey, signer), IBundleSwitchingService
{
    public BundleSwitchingService(string apiUrl, string consumerName, string privateKeyFile, string encryptedHmacSecretFile, string privateKeyFilePassword = null)
        : this(new Uri(apiUrl), consumerName, privateKeyFile, encryptedHmacSecretFile, privateKeyFilePassword) { }

    public BundleSwitchingService(Uri apiUrl, string consumerName, string privateKeyFile, string encryptedHmacSecretFile, string privateKeyFilePassword = null)
        : this(apiUrl, consumerName, ReadPrivateKeyFile(privateKeyFile, privateKeyFilePassword), encryptedHmacSecretFile) { }

    public BundleSwitchingService(string apiUrl, string consumerName, RSA privateKey, string encryptedHmacSecretFile)
        : this(new Uri(apiUrl), consumerName, privateKey, encryptedHmacSecretFile) { }

    public BundleSwitchingService(Uri apiUrl, string consumerName, RSA privateKey, string encryptedHmacSecretFile)
        : this(apiUrl, consumerName, HmacFromEncryptedBase64EncodedSecretFile(encryptedHmacSecretFile, privateKey), privateKey) { }

    public BundleSwitchingService(string apiUrl, string consumerName, HMACSHA256 signer, RSA privateKey)
        : this(new Uri(apiUrl), consumerName, signer, privateKey) { }

    public Task<HttpResponseMessage> SendConfirmationAsync(string id, CancellationToken cancellationToken = default)
    {
        var confirmationMessage = new ConfirmationMessage { TransactionId = id };
        return SendWithTokenAsync(HttpMethod.Put, apiUrl.AddPathArg($"dossiers/confirmations/{id}"), confirmationMessage, cancellationToken);
    }

    public async Task<MessageResponse> SendMessageAsync(IMessageEnvelope<IBsMessageContent> envelope, CancellationToken cancellationToken = default)
    {
        var responseMessage = await SendWithTokenAsync(HttpMethod.Post, apiUrl.AddPathArg($"dossiers/{TypeName(envelope)}"), envelope, cancellationToken).ConfigureAwait(false);
        var responseBody = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
        var json = JObject.Parse(responseBody);
        if (responseMessage.IsSuccessStatusCode)
        {
            return json.ToObject<MessageResponse>();
        }

        if (json.TryGetValue("transactionId", out _))
        {
            return json.ToObject<ErrorResponse>();
        }

        throw new HttpListenerException((int)responseMessage.StatusCode, responseBody);
    }
}