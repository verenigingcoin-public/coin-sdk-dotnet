using System.Collections.Generic;

namespace Coin.Sdk.WM.Messages.V1;

public enum MessageType
{
    WeasMigrationCancelV1,
    WeasMigrationRequestV1,
    WeasMigrationRequestAnswerV1,
    WeasMigrationErrorFoundV1
}

internal static class MessageTypeExtensions
{
    private static readonly Dictionary<MessageType, string> MessageTypeNames = new Dictionary<MessageType, string>
    {
        [MessageType.WeasMigrationCancelV1] = "weasmigrationcancel",
        [MessageType.WeasMigrationRequestV1] = "weasmigrationrequest",
        [MessageType.WeasMigrationRequestAnswerV1] = "weasmigrationrequestanswer",
        [MessageType.WeasMigrationErrorFoundV1] = "weasmigrationerrorfound"
    };

    internal static string Name(this MessageType messageType) => MessageTypeNames[messageType];
}