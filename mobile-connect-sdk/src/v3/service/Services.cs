using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Coin.Sdk.MC.v3.domain;

namespace Coin.Sdk.MC.v3.service;

public interface IMobileConnectService
{
    Task<DiscoveryResponse?> SendDiscoveryRequestAsync(
        string msisdn,
        string? correlationId = null,
        CancellationToken cancellationToken = default
    )
    {
        return SendDiscoveryRequestAsync(new DiscoveryRequest(msisdn, correlationId), cancellationToken);
    }

    Task<DiscoveryResponse?> SendDiscoveryRequestAsync(
        DiscoveryRequest discoveryRequest,
        CancellationToken cancellationToken = default
    );
}

public interface IMobileConnectClientErrorHandler
{
    void OnNotFound(DiscoveryRequest request, ErrorResponse errorResponse);

    void OnOtherError(DiscoveryRequest request, HttpResponseMessage responseMessage, string responseBody);
}