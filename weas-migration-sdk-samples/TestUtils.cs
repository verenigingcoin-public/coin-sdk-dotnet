using System;

namespace Coin.Sdk.WM.Sample;

public static class TestUtils
{
    public static string GenerateDossierId(string recipient, string donor) =>
        $"{recipient}-{donor}-{new Random().Next(10000, 99999)}-1";

    public static string GetPath(string relativePath) => $"../../../{relativePath}";
}