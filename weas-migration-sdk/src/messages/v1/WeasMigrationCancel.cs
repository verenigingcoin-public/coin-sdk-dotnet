using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Coin.Sdk.WM.Messages.V1;

public class WeasMigrationCancelMessage : IWmMessage<WeasMigrationCancel>
{
    [DataMember(Name = "header", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "header")]
    public Header Header { get; set; }

    [DataMember(Name = "body", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "body")]
    [JsonConverter(typeof(ConcreteConverter<CancelBody>))]
    public IWmMessageBody<WeasMigrationCancel> Body { get; set; }
}

public class CancelBody : IWmMessageBody<WeasMigrationCancel>
{
    [DataMember(Name = "cancel", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "cancel")]
    public WeasMigrationCancel Content { get; set; }
}

public class WeasMigrationCancel : IWmMessageContent
{
    /// <summary>
    /// The identifier for the WEAS Migration Request determined by the Recipient for communication between the Recipient and Donor. Defined as [RecipientSPCode]-[DonorSPCode]-[Identifier] The requirement is that the DossierId is unique and identifies the WEAS Migration Request. 
    /// </summary>
    [DataMember(Name = "dossierid", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "dossierid")]
    public string DossierId { get; set; }

    /// <summary>
    /// Note field for additional information
    /// </summary>
    [DataMember(Name = "note", EmitDefaultValue = false)]
    [JsonProperty(PropertyName = "note")]
    public string Note { get; set; }
}