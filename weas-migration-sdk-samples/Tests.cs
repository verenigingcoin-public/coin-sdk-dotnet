﻿using System;
using System.Collections.Generic;
using System.Threading;
using Coin.Sdk.Common.Client;
using Coin.Sdk.WM.Messages.V1;
using Coin.Sdk.WM.Service.Impl;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;
using static Coin.Sdk.WM.Sample.TestUtils;

namespace Coin.Sdk.WM.Sample;

public class Tests
{
    private WeasMigrationService _weasMigrationService = null!;
    private WeasMigrationMessageConsumer _messageConsumer = null!;

    private const string Sender = "<YOUR PROVIDER>";
    private const string Receiver = "<DONOR PROVIDER>";
    private readonly string _timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
    private const string PhoneNumber = "0612345678";

    [SetUp]
    public void Setup()
    {
        var logger = NullLogger.Instance;

        const string apiUrl = "https://test-api.coin.nl/weas-migration/v1";
        const string sseUrl = apiUrl + "/dossiers/events";
        const string consumer = "<YOUR CONSUMER>";
        var privateKeyFile = GetPath("private-key.pem");
        var encryptedHmacSecretFile = GetPath("sharedkey.encrypted");
        _weasMigrationService =
            new WeasMigrationService(apiUrl, consumer, privateKeyFile, encryptedHmacSecretFile);
        var sseConsumer = new SseConsumer(logger, consumer, sseUrl, privateKeyFile, encryptedHmacSecretFile, 1, 0);
        _messageConsumer = new WeasMigrationMessageConsumer(sseConsumer, logger);
    }

    [Test]
    public void SendWeasMigrationRequest()
    {
        var dossierId = GenerateDossierId(Sender, Receiver);
        Console.WriteLine($"Sending WEAS Migration Request with dossier id {dossierId}");

        var message = new MessageEnvelope<WeasMigrationRequest>
        {
            Message = new WeasMigrationRequestMessage
            {
                Header = new Header
                {
                    Sender = new Sender
                    {
                        ServiceProvider = Sender
                    },
                    Receiver = new Receiver
                    {
                        ServiceProvider = Receiver
                    },
                    Timestamp = _timestamp
                },
                Body = new WeasMigrationRequestBody
                {
                    Content = new WeasMigrationRequest
                    {
                        DossierId = dossierId,
                        RecipientServiceProvider = Sender,
                        DonorServiceProvider = Receiver,
                        EarlyTermination = "N",
                        Name = "Someone Withaname",
                        AddressBlock = new AddressBlock
                        {
                            Housenr = "123",
                            HousenrExt = "A",
                            Postcode = "1234AB"
                        },
                        NumberSeries = new List<NumberSeries>
                        {
                            new()
                            {
                                Start = PhoneNumber,
                                //End = PhoneNumber
                            }
                        }/*,
                        ValidationBlock = new List<ValidationBlock>
                        {
                            new ValidationBlock
                            {
                                Name = "contractid",
                                Value = "some contractid"
                            }
                        },
                        Note = "A note"*/
                    }
                }
            }
        };
        var response = _weasMigrationService.SendMessageAsync(message).Result;
        Console.WriteLine($"Transaction id: {response.TransactionId}");
        if (response is not ErrorResponse error) return;
        foreach (var content in error.Errors)
        {
            Console.WriteLine($"Error {content.Code}: {content.Message}");
        }

        Assert.Fail();
    }

    [Test]
    public void ConsumeMessages()
    {
        _messageConsumer.StartConsumingUnconfirmed(new Listener(), e => Assert.Fail("Disconnected"));
        Thread.Sleep(1000);
    }

    [Test]
    public void SendConfirmation()
    {
        const string messageId = "<ENTER MESSAGE ID>";
        var response = _weasMigrationService.SendConfirmationAsync(messageId).Result;
        Console.WriteLine(response.IsSuccessStatusCode
            ? $"Successfully sent confirmation of message {messageId}"
            : $"Confirmation failed with status {response.StatusCode}. Response: {response.Content.ReadAsStringAsync().Result}.");

        Assert.That(response.IsSuccessStatusCode);
    }
}