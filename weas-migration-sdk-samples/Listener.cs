using System;
using Coin.Sdk.WM.Messages.V1;
using Coin.Sdk.WM.Service;

namespace Coin.Sdk.WM.Sample;

public class Listener : IWeasMigrationMessageListener
{
    public void OnWeasMigrationCancel(string messageId, WeasMigrationCancelMessage message)
    {
        System.Diagnostics.Debug.WriteLine($"Received message with id {messageId} of type {message.GetType()}");
    }

    public void OnWeasMigrationErrorFound(string messageId, WeasMigrationErrorFoundMessage message)
    {
        System.Diagnostics.Debug.WriteLine($"Received message with id {messageId} of type {message.GetType()}");
    }

    public void OnException(Exception exception)
    {
        System.Diagnostics.Debug.WriteLine($"Exception received {exception}");
    }

    public void OnKeepAlive()
    {
        System.Diagnostics.Debug.WriteLine("Keepalive");
    }

    public void OnWeasMigrationRequest(string messageId, WeasMigrationRequestMessage message)
    {
        System.Diagnostics.Debug.WriteLine($"Received message with id {messageId} of type {message.GetType()}");
    }

    public void OnWeasMigrationRequestAnswer(string messageId, WeasMigrationRequestAnswerMessage message)
    {
        System.Diagnostics.Debug.WriteLine($"Received message with id {messageId} of type {message.GetType()}");
    }

    public void OnUnknownMessage(string messageId, string message)
    {
        System.Diagnostics.Debug.WriteLine($"Unknown message received: {message}");
    }
}