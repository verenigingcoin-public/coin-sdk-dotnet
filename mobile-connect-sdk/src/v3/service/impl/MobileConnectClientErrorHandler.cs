using System;
using System.Net;
using System.Net.Http;
using Coin.Sdk.MC.v3.domain;

namespace Coin.Sdk.MC.v3.service.impl;

public class MobileConnectClientErrorHandler : IMobileConnectClientErrorHandler
{
    public void OnNotFound(DiscoveryRequest request, ErrorResponse errorResponse)
    {
    }

    public void OnOtherError(DiscoveryRequest request, HttpResponseMessage responseMessage, string responseBody)
    {
        throw new HttpListenerException((int) responseMessage.StatusCode, responseBody);
    }
}